/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROL;

import DAO.ClienteIdao;
import Modelos.Clientes;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

/**
 *
 * @author victo
 */
public class IDaoClientes implements ClienteIdao {

    private RandomAccessFile raf;
    private File f;
    private final int SIZE = 10000;
    private String RAFNAME = "Clientes.dat";
    
     public IDaoClientes() {
        f = new File(RAFNAME);
    }
     
      public void Open() throws IOException {
        if (!f.exists()) {
            f.createNewFile();
            raf = new RandomAccessFile(f, "rw");
            raf.seek(0);
            raf.writeInt(0);//n
            raf.writeInt(0);//k
        } else {
            raf = new RandomAccessFile(f, "rw");
        }
    }

    public void Close() throws IOException {
        if (raf != null) {
            raf.close();
        }
    }
    
    
    
    @Override
    public List<Clientes> findById(String id) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save(Clientes t) throws IOException {
        Open();
        raf.seek(0);
        int n = raf.readInt();
        int k = raf.readInt();
        long pos = 8 + SIZE * k;
        raf.seek(pos);
        raf.writeInt(t.getID());
        raf.writeUTF(t.getNombre());
        raf.writeUTF(t.getDirección());
        raf.writeInt(t.getNúmeroTelefónico());
        raf.writeShort(t.getEdad());
        raf.writeUTF(t.getSexo());
        raf.seek(0);
        raf.writeInt(++n);
        raf.writeInt(++k);
        Close();
    
    }

    @Override
    public int update(Clientes t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
    
    }

    @Override
    public boolean delete(String numero) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Clientes> findAll() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
