/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROL;

import DAO.ClienteIdao;
import DAO.ProductoIDao;
import Modelos.Clientes;
import Modelos.Producto;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

/**
 *
 * @author victo
 */
public class IDaoProductos implements ProductoIDao {
    private RandomAccessFile raf;
    private File f;
    private final int SIZE = 10000;
    private String RAFNAME = "Productos.dat";
    
     public IDaoProductos() {
        f = new File(RAFNAME);
    }
     
      public void Open() throws IOException {
        if (!f.exists()) {
            f.createNewFile();
            raf = new RandomAccessFile(f, "rw");
            raf.seek(0);
            raf.writeInt(0);//n
            raf.writeInt(0);//k
        } else {
            raf = new RandomAccessFile(f, "rw");
        }
    }

    public void Close() throws IOException {
        if (raf != null) {
            raf.close();
        }
    }

    @Override
    public List<Clientes> findById(String id) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save(Producto t) throws IOException {
        Open();
        raf.seek(0);
        int n = raf.readInt();
        int k = raf.readInt();
        long pos = 8 + SIZE * k;
        raf.seek(pos);
        raf.writeUTF(t.getID());
        raf.writeUTF(t.getNombre());
        raf.seek(0);
        raf.writeInt(++n);
        raf.writeInt(++k);
        Close();
    }

    @Override
    public int update(Producto t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(String numero) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Producto> findAll() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
