/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Modelos.Clientes;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author victo
 */
public interface ClienteIdao extends IDao<Clientes> {
   List<Clientes>findById(String id)throws IOException; 
}
