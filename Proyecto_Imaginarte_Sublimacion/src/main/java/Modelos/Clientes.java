/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Sistemas-34
 */
public class Clientes {
    private int ID;
    private String Nombre;
    private String Dirección;
    private int númeroTelefónico;
    private short Edad;
    private String Sexo;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDirección() {
        return Dirección;
    }

    public void setDirección(String Dirección) {
        this.Dirección = Dirección;
    }

    public int getNúmeroTelefónico() {
        return númeroTelefónico;
    }

    public void setNúmeroTelefónico(int númeroTelefónico) {
        this.númeroTelefónico = númeroTelefónico;
    }

    public short getEdad() {
        return Edad;
    }

    public void setEdad(short Edad) {
        this.Edad = Edad;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String Sexo) {
        this.Sexo = Sexo;
    }
}
