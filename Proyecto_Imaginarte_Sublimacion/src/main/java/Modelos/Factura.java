/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.Date;

/**
 *
 * @author Sistemas-34
 */
public class Factura {
    private int númeroFactura;
    private Date Fecha;
    private double Monto;
    private int Cantidad;
    private String Productos;

    public int getNúmeroFactura() {
        return númeroFactura;
    }

    public void setNúmeroFactura(int númeroFactura) {
        this.númeroFactura = númeroFactura;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }

    public double getMonto() {
        return Monto;
    }

    public void setMonto(double Monto) {
        this.Monto = Monto;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    public String getProductos() {
        return Productos;
    }

    public void setProductos(String Productos) {
        this.Productos = Productos;
    }
}
